package com.bitmobix.core.base

import android.os.Bundle
import androidx.navigation.Navigator

sealed class Screens(var bundle: Bundle? = null, var extras: Navigator.Extras? = null) {
    class UserScreen : Screens() {
        companion object {
            val EXTRA_USERMODEL = "EXTRA_USERMODEL"
        }
    }

    class AlbumScreen : Screens()
}