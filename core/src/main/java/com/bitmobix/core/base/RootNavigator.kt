package com.bitmobix.core.base

interface RootNavigator {
    fun navigate(screen: Screens)
    fun getRootContainerID(): Int
}