package com.bitmobix.core.recyclerDecoration

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class CustomPositionItemDecoration : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val itemPosition = parent.getChildAdapterPosition(view)
        if (itemPosition == RecyclerView.NO_POSITION) return
        val itemCount = state.itemCount

        if (itemPosition == 0) {
            outRect.set(0, 0, 0, 8)
        } else if (itemCount > 0 && itemPosition == itemCount - 1) {
            outRect.set(32, 8, 32, 0)
        } else {
            outRect.set(32, 16, 32, 16)
        }
    }
}