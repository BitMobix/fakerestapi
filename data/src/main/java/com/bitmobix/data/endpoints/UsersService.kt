package com.bitmobix.data.endpoints

import com.bitmobix.data.model.UserResponse
import retrofit2.http.GET

interface UsersService {
    @GET("/users")
    suspend fun getUsers(): List<UserResponse>
}