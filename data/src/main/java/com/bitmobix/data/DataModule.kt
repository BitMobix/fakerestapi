package com.bitmobix.data

import com.bitmobix.data.repository.UserRepositoryImpl
import com.bitmobix.domain.repository.UserRepository
import org.koin.dsl.module
import retrofit2.Retrofit

object DataModule {
    val datamodule = module {
        single<UserRepository> { UserRepositoryImpl(get()) }
        single<Retrofit> { NetworkConfig.createRetrofit() }
    }
}