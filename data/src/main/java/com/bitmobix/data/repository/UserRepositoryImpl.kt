package com.bitmobix.data.repository

import com.bitmobix.domain.model.UserModel
import com.bitmobix.domain.repository.UserRepository
import com.bitmobix.data.NetworkConfig
import com.bitmobix.data.endpoints.UsersService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Retrofit

class UserRepositoryImpl(
    private val retrofit: Retrofit,
) : UserRepository {
    override suspend fun getUsers(): List<UserModel> = withContext(Dispatchers.IO) {
        retrofit.create(UsersService::class.java)
            .getUsers()
            .map {
                with(it) {
                    UserModel(id, name, phone, email, username, website)
                }
            }
    }
}