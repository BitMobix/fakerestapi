package com.bitmobix.feature_dashboard.ui

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.navigation.fragment.FragmentNavigatorExtras
import com.bitmobix.core.recyclerDecoration.CustomPositionItemDecoration
import com.bitmobix.core.base.BaseFragment
import com.bitmobix.core.base.Screens
import com.bitmobix.feature_dashboard.adapter.UsersAdapter
import com.bitmobix.feature_dashboard.databinding.FragmentTabUsersBinding
import com.google.android.material.transition.Hold
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class TabUsersFragment : BaseFragment<FragmentTabUsersBinding>(FragmentTabUsersBinding::inflate) {
    private val vm: DashboardViewModel by sharedViewModel()
    private val usersAdapter by lazy {
        UsersAdapter { view, userModel ->
            requireParentFragment().requireParentFragment().exitTransition = Hold().setDuration(350L)
            rootNavigator.navigate(Screens.UserScreen().apply {
                bundle = bundleOf(Screens.UserScreen.EXTRA_USERMODEL to userModel)
                extras = FragmentNavigatorExtras(view to "cardViewTransition")
            })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.getUsers()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recView.apply {
            this.adapter = usersAdapter
            addItemDecoration(CustomPositionItemDecoration())
        }
    }

    override fun onObservers() {
        vm.eventUsers.observe(viewLifecycleOwner, usersAdapter::submitList)
    }
}