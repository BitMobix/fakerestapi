package com.bitmobix.feature_dashboard.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bitmobix.domain.model.UserModel
import com.bitmobix.domain.repository.UserRepository
import kotlinx.coroutines.launch

class DashboardViewModel(
    private val userRepository: UserRepository
) : ViewModel() {
    val eventUsers = MutableLiveData<List<UserModel>>()

    fun getUsers() {
        viewModelScope.launch {
            val users = userRepository.getUsers()
            val newUsers = mutableListOf<UserModel>()

            repeat(3) {
                newUsers.addAll(
                    users.mapIndexed { index, userModel ->
                        userModel.copy(index = index)
                    }
                )
            }

            eventUsers.value = newUsers
        }
    }
}