package com.bitmobix.feature_dashboard.ui

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.bitmobix.core.base.BaseFragment
import com.bitmobix.core.base.Screens
import com.bitmobix.feature_dashboard.databinding.FragmentTabAlbumsBinding
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class TabAlbumsFragment : BaseFragment<FragmentTabAlbumsBinding>(FragmentTabAlbumsBinding::inflate) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.root.setOnClickListener {
            rootNavigator.navigate(Screens.AlbumScreen())
        }
    }
}