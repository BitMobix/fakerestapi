package com.bitmobix.feature_dashboard.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bitmobix.domain.model.UserModel
import com.bitmobix.feature_dashboard.HelperAnimator
import com.bitmobix.feature_dashboard.databinding.ItemUserBinding
import com.bitmobix.feature_dashboard.databinding.ItemUserHeaderBinding


class UsersAdapter(private val onClickItem: (View, UserModel) -> Unit) : ListAdapter<UserModel, RecyclerView.ViewHolder>(
    object : DiffUtil.ItemCallback<UserModel>() {
        override fun areItemsTheSame(oldItem: UserModel, newItem: UserModel): Boolean {
            return oldItem.index == newItem.index
        }

        override fun areContentsTheSame(oldItem: UserModel, newItem: UserModel): Boolean {
            return oldItem == newItem
        }
    }
) {

    enum class Type { HEADER, ITEM }

    class HeaderViewHolder(val binding: ItemUserHeaderBinding) : RecyclerView.ViewHolder(binding.root)

    class ItemViewHolder(val binding: ItemUserBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            Type.HEADER.ordinal -> {
                HeaderViewHolder(ItemUserHeaderBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            }
            else -> {
                ItemViewHolder(ItemUserBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            }
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position) ?: return

        when (holder) {
            is HeaderViewHolder -> {
                holder.binding.tvTitle.text = itemCount.toString()
            }
            is ItemViewHolder -> {
                holder.binding.apply {
                    ViewCompat.setTransitionName(root, "cardViewTransition$position")

                    tvName.text = "Name: ${item.name}"
                    tvUserName.text = "UserName: ${item.username}"

                    tvPhone.text = "Phone: ${item.phone}"
                    tvWebsite.text = "Website: ${item.website}"
                    tvEmail.text = "Email: ${item.email}"

                    imgArrow.rotation = if (item.isCollapse) 0f else 180f
                    layoutExpanded.visibility = if (item.isCollapse) View.GONE else View.VISIBLE

                    imgArrow.setOnClickListener {
                        val itemAdapter = getItem(holder.adapterPosition)
                        toggleExpandableLayout(itemAdapter.isCollapse, it, layoutExpanded, itemAdapter)
                    }

                    root.setOnClickListener {
                        onClickItem(it, item)
                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int = if (position == 0) Type.HEADER.ordinal else Type.ITEM.ordinal

    private fun toggleExpandableLayout(isCollapse: Boolean, view: View, layoutExpand: ViewGroup, userModel: UserModel) {
        HelperAnimator.toggleArrow(view, isCollapse)
        HelperAnimator.toggleLayout(layoutExpand, isCollapse)
        userModel.isCollapse = !isCollapse
    }
}
