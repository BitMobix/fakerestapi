package com.bitmobix.feature_dashboard

import com.bitmobix.feature_dashboard.ui.DashboardViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object DashboardModule {
    val dashboardModule = module {
        viewModel { DashboardViewModel(get()) }
    }
}