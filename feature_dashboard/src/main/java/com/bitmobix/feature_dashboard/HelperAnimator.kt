package com.bitmobix.feature_dashboard

import android.animation.ValueAnimator
import android.view.View
import android.view.View.MeasureSpec
import android.view.ViewGroup
import androidx.core.animation.doOnEnd


object HelperAnimator {
    private enum class Type {
        EXPAND, COLLAPSE
    }

    fun toggleLayout(view: ViewGroup, isCollapse: Boolean) {
        val typeToogle = if (isCollapse) Type.EXPAND else Type.COLLAPSE

        val widthSpec = MeasureSpec.makeMeasureSpec(view.rootView.width, MeasureSpec.AT_MOST)
        val heightSpec = MeasureSpec.makeMeasureSpec(view.rootView.width, MeasureSpec.AT_MOST)
        view.measure(widthSpec, heightSpec)
        val actualheight: Int = view.measuredHeight

        view.visibility = View.VISIBLE

        val anim = when (typeToogle) {
            Type.EXPAND -> ValueAnimator.ofInt(0, actualheight)
            Type.COLLAPSE -> ValueAnimator.ofInt(actualheight, 0)
        }

        anim.addUpdateListener { valueAnimator ->
            val valInt = valueAnimator.animatedValue as Int
            val layoutParams: ViewGroup.LayoutParams = view.layoutParams
            layoutParams.height = valInt
            view.layoutParams = layoutParams
        }

        if (typeToogle == Type.COLLAPSE) anim.doOnEnd { view.visibility = View.GONE }

        anim.duration = 300
        anim.start()
    }

    fun toggleArrow(view: View, isCollapse: Boolean) {
        view.animate().setDuration(300).apply {
            if (isCollapse) rotation(180f) else rotation(0f)
        }
    }
}