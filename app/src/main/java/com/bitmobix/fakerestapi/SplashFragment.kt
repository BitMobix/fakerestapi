package com.bitmobix.fakerestapi

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import java.util.Timer
import kotlin.concurrent.timerTask

class SplashFragment : Fragment(R.layout.fragment_splash) {
    var timer: Timer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        timer = Timer()
        timer?.schedule(timerTask {
            requireActivity().runOnUiThread {
                findNavController().navigate(R.id.action_splashFragment_to_graph_dashboard)
            }
        }, 1000)
    }

    override fun onDestroy() {
        timer?.cancel()
        timer = null
        super.onDestroy()
    }
}