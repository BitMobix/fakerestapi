package com.bitmobix.fakerestapi

import android.app.Application
import com.bitmobix.data.DataModule
import com.bitmobix.feature_dashboard.DashboardModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class RootApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@RootApplication)
            modules(
                DashboardModule.dashboardModule,
                DataModule.datamodule
            )
        }
    }
}