package com.bitmobix.fakerestapi

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.bitmobix.core.base.Screens
import com.bitmobix.core.base.RootNavigator
import com.bitmobix.feature_dashboard.ui.DashboardFragment

class RootActivity : AppCompatActivity(), RootNavigator {
    lateinit var navController: NavController
    private val fragmentListener = object : FragmentManager.FragmentLifecycleCallbacks() {
        override fun onFragmentStarted(fragmentManager: FragmentManager, fragment: Fragment) {
            super.onFragmentStarted(fragmentManager, fragment)
            val currentNavController = fragment.findNavController()

            val backStackEntry =
                fragmentManager.findFragmentById(R.id.navContainerRoot)?.childFragmentManager?.backStackEntryCount ?: 0

            supportActionBar?.apply {
                if (fragment !is DashboardFragment) {
                    title = currentNavController.currentDestination?.label
                }
                setDisplayHomeAsUpEnabled(backStackEntry > 0)
                setDisplayShowHomeEnabled(backStackEntry > 0)
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        supportFragmentManager.registerFragmentLifecycleCallbacks(fragmentListener, true)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        navController = (supportFragmentManager.findFragmentById(R.id.navContainerRoot) as NavHostFragment).navController
    }

    override fun onDestroy() {
        supportFragmentManager.unregisterFragmentLifecycleCallbacks(fragmentListener)
        super.onDestroy()
    }

    override fun navigate(screen: Screens) {
        when (screen) {
            is Screens.AlbumScreen -> {
                navController.navigate(R.id.action_dashboardFragment_to_albumFragment, screen.bundle)
            }
            is Screens.UserScreen -> {
                navController.navigate(
                    resId = R.id.action_dashboardFragment_to_userFragment,
                    args = screen.bundle,
                    navigatorExtras = screen.extras,
                    navOptions = null)
            }
        }
    }

    override fun getRootContainerID(): Int {
        return R.id.navContainerRoot
    }
}