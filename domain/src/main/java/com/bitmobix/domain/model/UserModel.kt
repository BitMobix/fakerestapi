package com.bitmobix.domain.model

import android.os.Parcelable
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserModel(
    val id: Int,
    val name: String,
    val phone: String,
    val email: String,
    val username: String,
    val website: String,
    var index: Int = 0,
) : Parcelable {
    @IgnoredOnParcel
    var isCollapse = true
}