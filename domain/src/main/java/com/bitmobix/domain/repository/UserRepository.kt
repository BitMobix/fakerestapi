package com.bitmobix.domain.repository

import com.bitmobix.domain.model.UserModel

interface UserRepository {
    suspend fun getUsers(): List<UserModel>
}