package com.bitmobix.feature_user

import android.os.Bundle
import android.view.View
import com.bitmobix.core.base.BaseFragment
import com.bitmobix.core.base.Screens
import com.bitmobix.domain.model.UserModel
import com.bitmobix.feature_user.databinding.FragmentUserBinding
import com.google.android.material.transition.MaterialContainerTransform

class UserFragment : BaseFragment<FragmentUserBinding>(FragmentUserBinding::inflate) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getParcelable<UserModel>(Screens.UserScreen.EXTRA_USERMODEL)?.let {
            with(binding) {
                tvEmail.text = it.email
                tvUserName.text = it.username
                tvName.text = it.name
                tvPhone.text = it.phone
                tvWebsite.text = it.website
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = MaterialContainerTransform().apply {
            drawingViewId = rootNavigator.getRootContainerID()
            duration = 400L
        }
    }
}